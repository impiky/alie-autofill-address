import os
import selenium
import configparser
import pyodbc
import time
import random
import subprocess
import sandboxie
import sys
import re


from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

VMCONFIG_FILE_PATH = r'C:\\abc\\ABCMOBI.ini'

def getConfig(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
yzmuser = getConfig("vcode", "username", "config.ini")
yzmpassword = getConfig("vcode", "password", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
# vmid = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)
# internet_login = getConfig("internet", "login", "config.ini")
# internet_pwd = getConfig("internet", "password", "config.ini")

# print(internet_login)
# print(internet_pwd)


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("execute failed")
            return False
        result = self.cur.fetchone()
        self.close()
        return result
    
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


class Browser:

    def __init__(self, byid, useragent):
        self.options = ChromeOptions()
        self.options.add_argument('log-level=3')
        self.options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.options.add_argument('user-agent={}'.format(useragent))
        self.options.add_argument(r"user-data-dir=" + "C:\\chromedata\\" + byid)
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(chromepath, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)
        # self.driver.get(URL)

    def close(self):
        self.driver.close()


def get_all_buyer_id(db_manager):
    return db_manager.fetchall("SELECT BuyerID FROM Buyer")


def get_alie_data(db_manager, buyer_id):
    return db_manager.fetchone("""SELECT ENFirstName, ENLastName, ENMiddleName, PostIndex, BuyerState, BuyerDistrict, BuyerCity, BuyerDeliveryAddress,
     AliexpressAccount, AliexpressPassword, Tel, BrowserUserAgent FROM Buyer WHERE BuyerID='{}'""".format(buyer_id))


def get_status_data(db_manager, buyer_id):
    return db_manager.fetchone("""SELECT AddressStatus, AccountStatus FROM Buyer WHERE BuyerID='{}'""".format(buyer_id))

def send_keys_delay_random(controller,keys,min_delay=0.05,max_delay=0.25):
    for key in keys:
        controller.send_keys(key)
        time.sleep(random.uniform(min_delay,max_delay))


def login_to_aliexpress(db_manager, browser, alie_login, alie_pwd, byid):
    browser.driver.get("https://login.aliexpress.com/")
    browser.driver.switch_to.frame("alibaba-login-box")
    var = 0
    while var != 1:
        try: 
            if browser.driver.find_element_by_xpath("//*[@id=\"login\"]/div[1]/div").get_attribute("class") != None:
                print("Already logined")
                browser.driver.find_element_by_xpath("//*[@id=\"login\"]/div/div/div[3]/button").click()
                browser.wait.until(EC.url_to_be("https://ru.aliexpress.com/"))
                db_manager.execute("UPDATE Buyer SET AccountStatus='WORKING' WHERE BuyerID='{}'".format(byid))
                var == 1
                return False
                
        except:
            pass
        try:
            # 输入账号 / input login
            browser.driver.find_element_by_xpath("//*[@id=\"fm-login-id\"]").clear()
            send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"fm-login-id\"]"), alie_login)
            # 输入密码 / input pwd
            # browser.driver.execute_script("document.querySelector('#fm-login-password').value='{}';".format(alie_pwd))
            send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"fm-login-password\"]"), alie_pwd)
            # click on login button
            browser.driver.find_element_by_class_name("fm-btn").find_element_by_class_name("fm-button").click()
        except:
            if browser.driver.current_url == "https://ru.aliexpress.com/":
                print("Login success")
                db_manager.execute("UPDATE Buyer SET AccountStatus='WORKING' WHERE BuyerID='{}'".format(byid))
                var == 1
                return True
            else:
                db_manager.execute("UPDATE Buyer SET AccountStatus='NOT WORKING' WHERE BuyerID='{}'".format(byid))
                time.sleep(1)
                return False


def fill_delivery_data(first_name, last_name, middle_name, post_index, state, district, city, delivery_address, telephone, browser, db_manager, byid):
    browser.driver.get("https://ilogisticsaddress.aliexpress.com/addressList.htm")
    try:
        existing_address = browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[2]/ul/li/div/div").get_attribute("class")
        if "sa-address-item" in existing_address:
            db_manager.execute("UPDATE Buyer SET AddressStatus='FILLED' WHERE BuyerID='{}'".format(byid))
            browser.close()
            return False
    except:
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"address-main\"]")))
        send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[1]/div/input"), last_name + " " + first_name + " " + middle_name)
        time.sleep(random.randint(1, 4))
        select = Select(browser.driver.find_element_by_name('country'))
        select.select_by_visible_text('Belarus')
        time.sleep(random.randint(1, 5))
        send_keys_delay_random( browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[3]/ul/li[1]/input"), delivery_address)
        time.sleep(random.randint(1, 10))
        send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[4]/div/input"), city)
        time.sleep(random.randint(1, 10))
        send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[5]/div/input"), "{} oblast, {} rajon".format(state, district))
        time.sleep(random.randint(1, 10))
        send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[6]/div/input"), post_index)
        time.sleep(random.randint(1, 10))
        send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[7]/ul/li/input[2]"), str(telephone).split("5", 1))
        time.sleep(random.randint(1, 10))
        browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[8]/label/input").click()
        time.sleep(random.randint(1, 3))
        browser.driver.find_element_by_xpath("//*[@id=\"address-main\"]/div[1]/div[12]/a[1]").click()
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"address-main\"]/div[2]/ul/li/div/div")))
        db_manager.execute("UPDATE Buyer SET AddressStatus='FILLED' WHERE BuyerID='{}'".format(byid))
        browser.close()


def run_sandboxed(byid):
    sbie = sandboxie.Sandboxie()
    sbie.create_sandbox(box=str(byid), options={'Enabled': 'yes'})
    sbie.start("script.exe", box=buyer_id, wait=False)


db_manager = DbManager()
all_buyers = get_all_buyer_id(db_manager)
for record in all_buyers:
    for buyer_id in record:
        first_name, last_name, middle_name, post_index, state, district, city, delivery_address, alie_login, alie_pwd, tel, user_agent = get_alie_data(db_manager, buyer_id)
        address_status, account_status = get_status_data(db_manager, buyer_id)
        if not (address_status and account_status) is None:
            continue
        if account_status == "NOT WORKING":
            continue
        else:
            browser = Browser(buyer_id, user_agent)
            status = login_to_aliexpress(db_manager, browser, alie_login, alie_pwd, buyer_id)
        if status == True:
            fill_delivery_data(first_name, last_name, middle_name, post_index, state, district, city, delivery_address, tel, browser, db_manager, buyer_id)
            browser.close()
            sys.exit(1)
        elif status == False:
            browser.close()
            sys.exit(1)
    
