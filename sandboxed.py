import sandboxie
import subprocess
import pyodbc
import configparser


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("execute failed")
            return False
        result = self.cur.fetchone()
        self.close()
        return result


def getConfig(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")


def get_buyer(db_manager):
    return db_manager.fetchone("SELECT BuyerID FROM Buyer WHERE AddressStatus='' AND AccountStatus=''")


db_manager = DbManager()
sbie = sandboxie.Sandboxie()
buyer_id = get_buyer(db_manager)
print(buyer_id)
sbie.create_sandbox(box=str(buyer_id), options={'Enabled': 'yes'})
sbie.start("script.exe", box=str(buyer_id), wait=False)
